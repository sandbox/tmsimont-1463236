<?php



/**
 * Form callback: Admin Settings form for notification
 */
function role_expire_notification_admin($form_state) {
	$form = array();
	
	//set up token help text fieldset
	if (module_exists('token')) {
		$token_help = array(
		  '#title' => t('Replacement patterns'),
		  '#type' => 'fieldset',
		  '#collapsible' => TRUE,
		  '#collapsed' => TRUE,
		  '#description'=>t("Tokens are available in all of the text inputs below"),
		  '#value' => theme('token_help', 'user'),
		  );
	}
		 
	//node title token replacement
	// if (module_exists('token')) {
		// $node_title = token_replace($node_title, 'user', $user);
	// }
		
	//get settings from DB
	$settings = role_expire_notification_get_email_settings();
	
	// drupal_set_message("<pre>".print_r($settings,true)."</pre>");
	// drupal_set_message("<pre>".print_r($form_state,true)."</pre>");
	
	//create a fieldset of email/userpage notification options for each existing role
	//TODO: is there a way to only get roles that are set up for expiration?
	foreach( _role_expire_get_role() as $rid => $role){
	
		//collapse email if setting is set to zero and no form state post, or form state post is set to zero, or no data at all exists for this rid in settings or post
		$email_collapsed = ((isset($settings[$rid])&&$settings[$rid]['notification_email_time']==0 && !isset($form_state['post']['emails:notification_email_time:'.$rid]))
			|| (isset($form_state['post']['emails:notification_email_time:'.$rid])&&$form_state['post']['emails:notification_email_time:'.$rid]==0)
			|| (!(isset($settings[$rid])) && !isset($form_state['post']['emails:notification_email_time:'.$rid])))?true:false;
		
		//collapse userpage if setting is set to zero and no form state post, or form state post is set to zero, or no data at all exists for this rid in settings or post
		$userpage_collapsed = ((isset($settings[$rid])&&$settings[$rid]['notification_userpage_time']==0 && !isset($form_state['post']['userpage:notification_userpage_time:'.$rid]))
			|| (isset($form_state['post']['userpage:notification_userpage_time:'.$rid])&&$form_state['post']['userpage:notification_userpage_time:'.$rid]==0)
			|| (!(isset($settings[$rid])) && !isset($form_state['post']['userpage:notification_userpage_time:'.$rid])))?true:false;
		
		
		//main role container fieldset
		$form[$rid]= array(
			'#type'=>'fieldset',
			'#title'=>$role,
			'#collapsible'=>true,
			'#collapsed'=>$email_collapsed && $userpage_collapsed
		);
		//pass user friendly role name for messages displayed to user (validation)
		$form[$rid]['role:'.$rid] = array(
			'#type'=>'hidden',
			'#value'=> $role
		);
		
		
		/**
		 *  begin: email settings
		 */
		 
			
		//wrap email fields in fieldset
		$form[$rid]['emails']['#type']='fieldset';
		$form[$rid]['emails']['#title']='Email settings';	
		$form[$rid]['emails']['#collapsible']=true;
		$form[$rid]['emails']['#collapsed']=$email_collapsed;
		
		$form[$rid]['emails']['emails:notification_email_time:'.$rid] = array(
			'#type' => 'select',
			'#title' => t('Time between email notification and role expiration'),
			'#default_value' => isset($settings[$rid]['notification_email_time'])?$settings[$rid]['notification_email_time']:0,
			'#options' => array(
				0 => t("Do not send notification"),
				'86400' => t("1 day"),
				'259200' => t("3 days"),
				'604800' => t("1 week"),
				'1209600' => t("2 weeks"),
				'2592000' => t("1 month"),
				'7776000' => t("3 months")
			),
			'#description' => t("This much time will pass after the notification is sent before the role is expired.")
		);

		$form[$rid]['emails']['token'] = $token_help;
		
		$form[$rid]['emails']['emails:subject:'.$rid] = array(
			'#type'=>'textfield',
			'#maxlength'=>100,
			'#default_value' => isset($settings[$rid]['notification_email_subject'])?$settings[$rid]['notification_email_subject']:'',
			'#title'=>t("Subject"),
		);
		$form[$rid]['emails']['emails:from_address:'.$rid] = array(
			'#type'=>'textfield',
			'#maxlength'=>100,
			'#default_value' => isset($settings[$rid]['notification_email_addresses']['from'])?$settings[$rid]['notification_email_addresses']['from']:'',
			'#title'=>t("From"),
		);
		$form[$rid]['emails']['emails:to_address:'.$rid] = array(
			'#type'=>'textfield',
			'#maxlength'=>100,
			'#default_value' => isset($settings[$rid]['notification_email_addresses']['to'])?$settings[$rid]['notification_email_addresses']['to']:'',
			'#title'=>t("To"),
		);
		$form[$rid]['emails']['emails:email_body:'.$rid] = array(
			'#type'=>'textarea',
			'#maxlength'=>255,
			'#default_value' => isset($settings[$rid]['notification_email_body'])?$settings[$rid]['notification_email_body']:'',
			'#title'=>t("Body"),
		);
		
		/**
		 *  end: email settings
		 */
		 
		 
			
		/**
		 *  begin: userpage settings
		 */
		 
		//wrap email fields in fieldset
		$form[$rid]['userpage']['#type']='fieldset';
		$form[$rid]['userpage']['#title']='User page settings';	
		$form[$rid]['userpage']['#collapsible']=true;
		$form[$rid]['userpage']['#collapsed']=$userpage_collapsed;
		
		$form[$rid]['userpage']['userpage:notification_userpage_time:'.$rid] = array(
			'#type' => 'select',
			'#title' => t('Time before expiration to show notification'),
			'#default_value' => isset($settings[$rid]['notification_userpage_time'])?$settings[$rid]['notification_userpage_time']:0,
			'#options' => array(
				0 => t("Do not show notification"),
				'86400' => t("1 day"),
				'259200' => t("3 days"),
				'604800' => t("1 week"),
				'1209600' => t("2 weeks"),
				'2592000' => t("1 month"),
				'7776000' => t("3 months")
			),
			'#description' => t("This much time before the role expires, a message will appear on the user page and will not go away until the notification is cleared from the database.")
		);

		$form[$rid]['userpage']['token'] = $token_help;
		
		$form[$rid]['userpage']['userpage:notification_userpage_body:'.$rid] = array(
			'#type'=>'textarea',
			'#maxlength'=>255,
			'#default_value' => isset($settings[$rid]['notification_userpage_body'])?$settings[$rid]['notification_userpage_body']:'',
			'#title'=>t("Message body"),
		);
		
		/**
		 *  end: userpage settings
		 */
		
	}

	$form['buttons']['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save configuration'),
	);	
	$form['#submit'][] = 'role_expire_notification_settings_submit';
	$form['#validate'][] = 'role_expire_notification_settings_validate';
	$form['#theme'] = 'system_settings_form';
	
	return $form;
}

/** 
 * validate settings form
 */
function role_expire_notification_settings_validate(&$form, &$form_state) {
	$parsed = role_expire_notification_settings_parse($form_state['values']);
	foreach($parsed as $rid => $values){
		//if a notification time was set for email, then make all email fields required
		if($values['emails']['notification_email_time']!=0){
			foreach($values['emails'] as $val){
				if($val==''){
					form_set_error('emails:notification_email_time:'.$rid, "An email notification cannot be sent without all of the email fields filled out for role <i>".$form_state['values']['role:'.$rid]."</i>");
				}
			}
		}
	}
	foreach($parsed as $rid => $values){
		//if a notification time was set for userpage, then make the userpage message required
		if($values['userpage']['notification_userpage_time']!=0){
			foreach($values['userpage'] as $val){
				if($val==''){
					form_set_error('userpage:notification_userpage_time:'.$rid, "If a message is to be displayed, that message must be defined for role <i>".$form_state['values']['role:'.$rid]."</i>");
				}
			}
		}
	}
}

/**
 * Form submit callback: save role expire notification settings
 */
function role_expire_notification_settings_submit(&$form, &$form_state) {
	$fail = false;
	$parsed = role_expire_notification_settings_parse($form_state['values']);
	foreach($parsed as $rid => $values){
		if(!role_expire_notification_save_setting($rid, $values))
			$fail = true;
	}
	if($fail) {
		drupal_set_message(t("There was a problem saving your settings."),'error');
	}else {
		drupal_set_message(t("Your settings have been saved."));
	}
}

/**
 *  Helper function: Parse nested field values based on key, which is fieldset:KEY:rid
 */
function role_expire_notification_settings_parse($values){
	$parsed = array();
	foreach($values as $key=>$value){
		if(substr($key,0,7)=='emails:'){
			$vals = explode(':',$key);
			$k = $vals[1];
			$rid = $vals[2];
			$parsed[$rid]['emails'][$k]=$value;
		}
		if(substr($key,0,9)=='userpage:'){
			$vals = explode(':',$key);
			$k = $vals[1];
			$rid = $vals[2];
			$parsed[$rid]['userpage'][$k]=$value;
		}
	}
	return $parsed;
}

/**
 * Get: settings from the database
 */
function role_expire_notification_get_email_settings() {
	$settings = array();
	$result = db_query("SELECT * FROM {role_expire_notification_settings}");
	while($row = db_fetch_array($result)){
		$settings[$row['rid']] = array(
			'notification_email_time' => $row['notification_email_time'],
			'notification_email_body' => $row['notification_email_body'],
			'notification_email_subject' => $row['notification_email_subject'],
			'notification_email_addresses' => unserialize($row['notification_email_addresses']),
			'notification_userpage_time' => $row['notification_userpage_time'],
			'notification_userpage_body' => $row['notification_userpage_body'],
		);
	}
	return $settings;
}

/**
 * Set: settings to the database
 */
function role_expire_notification_save_setting($rid, $values){
	$saved = false;
	$result = db_result(db_query("SELECT COUNT(*) FROM {role_expire_notification_settings} WHERE rid=%d",$rid));
	if($result){
		$saved = db_query("UPDATE {role_expire_notification_settings} SET notification_email_time=%d, notification_email_body='%s', notification_email_subject='%s', notification_email_addresses='%s', notification_userpage_time=%d, notification_userpage_body='%s' WHERE rid=%d",
			$values['emails']['notification_email_time'],$values['emails']['email_body'],$values['emails']['subject'],serialize(array('from'=>$values['emails']['from_address'],'to'=>$values['emails']['to_address'])),
			$values['userpage']['notification_userpage_time'],$values['userpage']['notification_userpage_body'],$rid);
	}else{
		$saved = db_query("INSERT INTO {role_expire_notification_settings} (notification_email_time,notification_email_body,notification_email_subject, notification_email_addresses,notification_userpage_time, notification_userpage_body,rid) VALUES (%d,'%s','%s','%s',%d, %d, '%s')",
			$values['emails']['notification_email_time'],$values['emails']['email_body'],$values['emails']['subject'],serialize(array('from'=>$values['emails']['from_address'],'to'=>$values['emails']['to_address'])),
			$values['userpage']['notification_userpage_time'],$values['userpage']['notification_userpage_body'],$rid);
	}
	
	return $saved;
}